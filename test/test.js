const assert = require('assert');
const getSmallestMultiple = require('../index.js');

describe('Problem4', function() {
  it('The smallest multiple from 1-20 is correct', function() {
    assert.equal(getSmallestMultiple(), 232792560);
  });
});
